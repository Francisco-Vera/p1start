package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;




public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{

	private static class Node<E>{
		private E element;
		private Node<E> next;
		private Node<E> prev;

		public Node() {
			this.element = null;
			this.next = this.prev = null;

		}
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		public Node<E> getPrev() {
			return prev;
		}
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}

	}

	private Node<E> header;
	private int currentSize;
	private Comparator<E> comp;

	public CircularSortedDoublyLinkedList(Comparator<E> cmp) {
		this.currentSize = 0;
		this.header = new Node<>();

		this.header.setNext(this.header);
		this.header.setPrev(this.header);

		this.comp = cmp;
	}

	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
		private Node<E> nextNode;
		private int index;

		@SuppressWarnings("unchecked")
		public CircularSortedDoublyLinkedListIterator() {
			this.nextNode = (Node<E>) header.getNext();
		}
		public CircularSortedDoublyLinkedListIterator(int i) {
			int count = 0;
			this.nextNode = (Node<E>) header;
			while(count <= i) {
			this.nextNode = this.nextNode.getNext();
			count++;
			}
			this.index = i;
		}
		@Override
		public boolean hasNext() {
			return nextNode.getElement() != null;
		} 

		@Override
		public E next() {
			if (this.hasNext()) {

				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}

	}
	
	private class CircularSortedDoublyLinkedListReverseIterator<E> implements Iterator<E>{
		private Node<E> nextNode;
		private int index;

		@SuppressWarnings("unchecked")
		public CircularSortedDoublyLinkedListReverseIterator() {
			this.nextNode = (Node<E>) header.getPrev();
		}
		@SuppressWarnings("unchecked")
		public CircularSortedDoublyLinkedListReverseIterator(int i) {
			int count = 0;
			this.nextNode = (Node<E>) header;
			while(count <= i) {
			this.nextNode = this.nextNode.getPrev();
			count++;
			}
			this.index = i;
		}
		@Override
		public boolean hasNext() {
			return nextNode.getElement() != null;
		} 

		@Override
		public E next() {
			if (this.hasNext()) {

				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getPrev();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}

	}

	public Iterator<E> iterator() {


		return new CircularSortedDoublyLinkedListIterator<E>();
	}
	public Iterator<E> iterator(int i) {
	return new CircularSortedDoublyLinkedListIterator<E>(i);
	}
	
	public Iterator<E> reverseiterator() {
	return new CircularSortedDoublyLinkedListReverseIterator<E>();
	}
	public Iterator<E> reverseiterator(int index) {
	return new CircularSortedDoublyLinkedListReverseIterator<E>(index);
	}



	@Override
	public boolean add(E obj)  { //method to add to the list
 		if(obj == null) { // if obj is null don't add 
			return false;
		}
		Node<E> newNode = new Node<E>();
		newNode.setElement(obj);
		if(this.isEmpty()) {  // if the list is empty add it as the fisrt element ( no need to sort)
			this.header.setNext(newNode);
			this.header.setPrev(newNode);
			newNode.setNext(this.header);
			newNode.setPrev(this.header);
			this.currentSize++;
			return true;
		}
		Node<E> temp = this.header.getNext();
		newNode.setElement(obj);
		boolean x = true;
		while(x) {  //use given comparator to sort through the list before adding the object
			if(this.comp.compare(temp.getElement(), newNode.getElement()) == 0) { // if the new elemnet is equal to the one it is, add it
				newNode.setPrev(temp.getPrev());
				temp.getPrev().setNext(newNode);
				temp.setPrev(newNode);
				newNode.setNext(temp);
				this.currentSize++;
				return true;

			}
			else if(comp.compare(temp.getElement(), newNode.getElement()) < 0 && //if your at the end of the list add it 
					temp.getNext().getElement() == null) {

				newNode.setNext(header);
				this.header.setPrev(newNode);
				temp.setNext(newNode);
				newNode.setPrev(temp);
				this.currentSize++;
				return true;
			}
			else if(temp.getPrev().getElement() == null && comp.compare(temp.getElement(), newNode.getElement()) > 0) {
														//if the second element is bigger than new object, add it at beginning of list
				newNode.setNext(temp);
				temp.setPrev(newNode);
				header.setNext(newNode);
				newNode.setPrev(header);
				this.currentSize++;
				return true;
			}

			else if(comp.compare(temp.getElement(), newNode.getElement()) < 0  && 
					comp.compare(temp.getNext().getElement(), newNode.getElement()) > 0 ) {
										//if the element your on is smaller than the new one and the next one is bigger, add them in between
				newNode.setNext(temp.getNext());
				temp.getNext().setPrev(newNode);
				temp.setNext(newNode);
				newNode.setPrev(temp);
				this.currentSize++;
				return true;
			}

			else if(temp.getNext().getElement() == null) { //if your at the end of the list add it 
				temp.setNext(newNode);
				newNode.setPrev(temp);
				newNode.setNext(this.header);
				this.header.setPrev(newNode);
				this.currentSize++;
				return true;
			}



			temp = temp.getNext(); // moves list along

		}
		return false;

	}

	@Override
	public int size() {

		return this.currentSize; // returns the size of list
	}

	@Override
	public boolean remove(E obj) {
		if(obj == null) {
			return false;
		}
		Node<E> temp = this.header.getNext();

		while(true) { 
			if(this.comp.compare(temp.getElement(), obj) == 0) { //looks for the object equal to the one given with the compare method
				temp.getPrev().setNext(temp.getNext());			// and removes it
				temp.getNext().setPrev(temp.getPrev());
				temp.setNext(null);
				temp.setPrev(null);
				temp.setElement(null);
				this.currentSize--;
				return true; 
			}
			else if(temp.getNext().getElement() == null) { //return false if object wasnt found 
				return false;
			}
			temp = temp.getNext();
		}


	} 

	@Override
	public boolean remove(int index) {
		if(index < 0 || this.size() <= index) { //if index is negative or bigger than the list size give an error
			throw new IndexOutOfBoundsException();
		}
		Node<E> temp = this.header.getNext();
		while(index != 0) {  //looks for the object at that index in the list
			temp = temp.getNext();
			index--;
		}

		temp.getPrev().setNext(temp.getNext());
		temp.getNext().setPrev(temp.getPrev());
		temp.setNext(null);                       // removes the object
		temp.setPrev(null);
		temp.setElement(null);
		this.currentSize--;
		return true; 
	}

	@Override
	public int removeAll(E obj) { 
		int count = 0;
		while (this.remove(obj)) {  //removes all of said objects on the list while keeping count 
			count++;
		}
		return count;
	}

	@Override
	public E first() {

		return this.header.getNext().getElement(); // returns the first element
	}

	@Override 
	public E last() {
		// TODO Auto-generated method stub
		return this.header.getPrev().getElement();  //returns the last element
	}

	@Override
	public E get(int index) {
		if(index < 0 || this.size() <= index) {  //if index is negative or bigger than the list size give an error
			throw new IndexOutOfBoundsException();
		}
		Node<E> result = this.header.getNext();
		while(index != 0) {   //goes through the list until it reaches index
			result = result.getNext();
			index--;
		} 

		return result.getElement();  // returns element at index
	}

	@Override
	public void clear() { 
		while(!this.isEmpty()) { //keeps removing while it can
			this.remove(0);
		}

	}

	@Override
	public boolean contains(E e) {

		if(this.firstIndex(e)!= -1) { //uses first index to see if object is in list
			return true;
		}

		return false;

	}

	@Override
	public boolean isEmpty() {
		if(this.size() == 0) {
			return true;
		}
		return false;
	}

	@Override
	public int firstIndex(E e) {

		Node<E> temp = this.header.getNext();
		int count = 0;
		while(true) { //iterates over list 

			if(this.comp.compare(temp.getElement(), e) == 0) { //if object is found return its index
				return count;
			}
			else if(temp.getNext().getElement() == null) {
				return -1;
			} 
			count++;
			temp = temp.getNext();
		}


	}

	@Override
	public int lastIndex(E e) {
		Node<E> temp = this.header.getPrev();
		int count = this.currentSize - 1;
		while(true) {//iterates over list

			if(this.comp.compare(temp.getElement(), e) == 0) {
				return count;
			}
			else if(temp.getPrev().getElement() == null) {//if object is found return its index
				return -1;
			} 
			count--;
			temp = temp.getPrev(); // moves list along
		}
	}

}
