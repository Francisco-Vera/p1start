
package edu.uprm.cse.datastructures.cardealer.model;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/cars")
public class CarManager {

	private static CircularSortedDoublyLinkedList<Car> cList = CarList.getInstance();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		Car[] result = new Car[cList.size()];
		for (int i = 0; i < cList.size(); i++) { // iterates over list adding each car to an array
			result[i] = cList.get(i);
		}
		return result; //returns array 
	}
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){
		for(Car result : cList) { //iterates over list looking for the car with the same id
			if(result.getCarId() == id) {
				return result;
			}
		}
		throw new NotFoundException(new JsonError("Error", "Car" + id + "not found"));

	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) {
		for (Car temp : cList) {
			if(temp.getCarId() == car.getCarId()) { //checks if there's already a car with the same id as new car
				return Response.status(Response.Status.CONFLICT).build(); 
			}
		}
		cList.add(car); //adds car
		return Response.status(201).build();
	}

	@DELETE
	@Path("/{id}/delete")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteCar(@PathParam("id") long id) {
		for (Car car : cList) {
			if(car.getCarId() == id) { //looks for car with same id to remove it from list
				cList.remove(car);
				return Response.status(Response.Status.OK).build();
			}

		}

		return Response.status(Response.Status.NOT_FOUND).build(); //return error if car wasn't found



	}

	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car) {
		for (Car replace : cList) {
			if(replace.getCarId() == car.getCarId()) { //looks for car with same id to remove it from list and add it bac in with the new information 
				cList.remove(replace);
				cList.add(car);
				return Response.status(Response.Status.OK).build();
			}
			
		}
		return Response.status(Response.Status.NOT_FOUND).build();//return error if car wasn't found
	}





}