package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>{

	@Override
	public int compare(Car o1, Car o2) {
		if(o1.equals(o2)) { // if the cars are the same return 0
			return 0;
		}
		else if(o1.getCarBrand().compareTo(o2.getCarBrand()) == 0) {// first check if the cars have the same brand
			if(o1.getCarModel().compareTo(o2.getCarModel())== 0) {//check if the cars have the same model
				return o1.getCarModel().compareTo(o2.getCarModelOption());//return the comparison between the model options
			}
			else {
				return o1.getCarModel().compareTo(o2.getCarModel());//return the comparison between the models
			}
			
		}
		else {
			return o1.getCarBrand().compareTo(o2.getCarBrand());//return the comparison between the brands
		}
		
	}

}
